import argparse

import transcript_sampler as ts

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Read,sample and write transcript")
    parser.add_argument("-f", "--file",         type=str, metavar="", required=True,  help="Path file to read")
    parser.add_argument("-wf", "--writeToFile", type=str, metavar="", required=False, help="Path file to write")
    parser.add_argument("-rn", "--number",      type=int, metavar="", required=False, help="Random expression number")
    args = parser.parse_args()

    t1 = ts.TranscriptSampler()
    t1.main(args.file)
