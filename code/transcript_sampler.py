import numpy as np

class TranscriptSampler:

    def read_avg_expression(self,file):
        """reads a file in the format "gene_id nr_copies"
            constructs a dictionary with the average number of transcripts for every gene (i.e. nr_copies)
            return this dictionary"""
        gene_expression_dict = {}
        with open(file, "r") as my_file:
            for line in my_file:
                stripped_line = line.strip()
                column = stripped_line.split()
                gene_expression_dict[column[0]] = column[1]
        return gene_expression_dict


    def sample_transcripts(self,avgs, number):
        """takes as input the dictionary constructed above and a total number of transcripts to sample
            it generates a sample of transcripts by sampling from individual genes in accordance to the relative abundance computed from the input dictionary
            returns the dictionary built from this sample"""
        sample_dict = {}
        for (k, v) in avgs.items():
            sample_dict[k] = int(v) * number
        return (sample_dict)

    def write_sample(self,file, sample):
        """takes as input a file name and a sample dictionary (constructed as described at point 2)
            writes to the file the sample dictionary"""
        with open(file, "w") as f:
            for (k, v) in sample.items():
                f.write("".join([k, ':\t', str(v), "\n"]))

    def main(self,file, number= None,writeToFile= None ):
        avg_expression = self.read_avg_expression(file)
        print(avg_expression)

        if number == None:
            number = np.random.randint(1,1000)
        sample_trancr = self.sample_transcripts(avg_expression, number)
        print(sample_trancr)

        if writeToFile == None:
            writeToFile="test_sample.fasta"
        self.write_sample(writeToFile, sample_trancr)