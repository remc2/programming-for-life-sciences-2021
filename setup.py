from setuptools import setup, find_packages

setup(
    name='transcriptSampler_RM',
    url='https://gitlab.com/remc2/programming-for-life-sciences-2021.git',
    author='Ruth Montano',
    author_email='r.montanocrespo@unibas.ch',
    description='This package helps to read, sample and write transcript sequences',
    license='MIT',
    version='1.0.0',
    packages=find_packages(),  # this will autodetect Python packages from the directory tree, e.g., in `code/`
    install_requires=[],  # add here packages that are required for your package to run, including version or range of versions

    entry_points = {
        'console_scripts': ['my-executable=transcriptSampler_RM.cli:main']} #Create command-line executables from your code
)